<?php

function validateLog() {

	$errors = [];
			$file_name = $_FILES['logFile']['name'];
			$file_size = $_FILES['logFile']['size'];
			$file_tmp = $_FILES['logFile']['tmp_name'];
			$file_type = $_FILES['logFile']['type'];
			$file_ext = strtolower(end (explode('.', $_FILES['logFile']['name'] ) ) );

			$extension = ['txt']; // setting as array incase we decide to add more extensions

			if (in_array ($file_ext, $extension) === false) {
				$errors[] = "Extension not allowed, please choose a .txt file";
			}

			if ($file_size > 10485760) {
				$errors[] = "Log must be less than 10MB";
			}

			if(empty ($errors) == true) {
				move_uploaded_file($file_tmp,"logs/".$file_name);
				echo "Results";
				$testLog = new logTest();
			} else {
				echo "There was a problem with uploading your log file";
			}
}

function h($string) {
	return htmlspecialchars($string);
}


?>