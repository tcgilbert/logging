<?php require_once('../private/initialize.php'); ?>

<?php
	if (isset ($_FILES['logFile'] ) ) {
		validateLog();
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Logging</title>
	<meta charset="utf-8">
	<link rel="stylesheet" media="all" href="stylesheets/main.css">
</head>

<body>
	<div class="content">
	<h1>Logging</h1>
		<form action="<?= $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
			<input type="file" name="logFile"><br>
			<input type="submit" value="Upload Log">
		</form>
	</div>
</body>
</html>